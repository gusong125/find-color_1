﻿// CDialog2.cpp: 实现文件
//

#include "pch.h"
#include "FindColorTool.h"
#include "CDialog2.h"
#include "afxdialogex.h"

// CDialog2 对话框

IMPLEMENT_DYNAMIC(CDialog2, CDialogEx)

CDialog2::CDialog2(CWnd* pParent /*=nullptr*/)
	:m_isDrawCube(FALSE)
	, m_dialog1Hwnd(NULL)
	, m_toolHwnd(NULL)
	, m_rangeSize(0)
	, m_targerDC(NULL)
	, CDialogEx(IDD_DIALOG2, pParent)
{

}

CDialog2::~CDialog2()
{
}

void CDialog2::DoDataExchange(CDataExchange* pDX)
{
	RegisterHotKey(this->m_hWnd, 21, NULL, VK_UP);
	RegisterHotKey(this->m_hWnd, 22, NULL, VK_DOWN);
	RegisterHotKey(this->m_hWnd, 23, NULL, VK_LEFT);
	RegisterHotKey(this->m_hWnd, 24, NULL, VK_RIGHT);
	RegisterHotKey(this->m_hWnd, 25, MOD_ALT, 'a');
	RegisterHotKey(this->m_hWnd, 25, MOD_ALT, 'A');
	RegisterHotKey(this->m_hWnd, 26, MOD_ALT, 's');
	RegisterHotKey(this->m_hWnd, 26, MOD_ALT, 'S');
	RegisterHotKey(this->m_hWnd, 1, NULL, VK_NUMPAD1);
	RegisterHotKey(this->m_hWnd, 2, NULL, VK_NUMPAD2);
	RegisterHotKey(this->m_hWnd, 3, NULL, VK_NUMPAD3);
	RegisterHotKey(this->m_hWnd, 4, NULL, VK_NUMPAD4);
	RegisterHotKey(this->m_hWnd, 5, NULL, VK_NUMPAD5);
	RegisterHotKey(this->m_hWnd, 6, NULL, VK_NUMPAD6);
	RegisterHotKey(this->m_hWnd, 7, NULL, VK_NUMPAD7);
	RegisterHotKey(this->m_hWnd, 8, NULL, VK_NUMPAD8);
	RegisterHotKey(this->m_hWnd, 9, NULL, VK_NUMPAD9);
	RegisterHotKey(this->m_hWnd, 10, NULL, VK_NUMPAD0);
	RegisterHotKey(this->m_hWnd, 11, MOD_ALT, VK_NUMPAD2);
	RegisterHotKey(this->m_hWnd, 12, MOD_ALT, VK_NUMPAD3);
	RegisterHotKey(this->m_hWnd, 13, MOD_ALT, VK_NUMPAD4);
	RegisterHotKey(this->m_hWnd, 14, MOD_ALT, VK_NUMPAD5);
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDialog2, CDialogEx)
	ON_WM_PAINT()
	ON_WM_MOUSEMOVE()
	ON_WM_HOTKEY()
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CDialog2 消息处理程序


void CDialog2::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 在此处添加消息处理程序代码
					   // 不为绘图消息调用 CDialogEx::OnPaint()
}


void CDialog2::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	RECT rect;
	HDC dc = ::GetDC(this->m_hWnd);
	GetWindowRect(&rect);
	int range = m_rangeSize / 2;
	int number = 0;
	tempRGB.clear();
	for (int i = point.y - range; i <= point.y + range; i++)
	{
		tempRGB.push_back(std::vector<RGBTRIPLE>());
		for (int j = point.x - range; j <= point.x + range; j++)
		{
			if (i<0 || j<0 || j>rect.right - rect.left || i>rect.bottom - rect.top)
			{
				tempRGB[number].push_back({ 0,0,0 });
				continue;
			}
			COLORREF rgb = GetPixel(dc, j, i);
			tempRGB[number].push_back({ GetBValue(rgb),GetGValue(rgb) ,GetRValue(rgb) });
		}
		number++;
	}
	m_curMousePt = point;
	::SendMessage(m_dialog1Hwnd, WM_MYMESSAGE, NULL, (LPARAM)this);

	if (m_isDrawCube)
	{
		BitBlt(dc, 0, 0, rect.right - rect.left, rect.bottom - rect.top, m_targerDC, 0, 0, SRCCOPY);
		HPEN brush = CreatePen(PS_SOLID, 1, RGB(255, 0, 0));
		SelectObject(dc, brush);
		Tool::DrawCube(dc, { m_cubeMousePt.x,m_cubeMousePt.y,point.x,point.y });
		DeleteObject(brush);
	}

	DeleteDC(dc);
	CDialogEx::OnMouseMove(nFlags, point);
}

void CDialog2::OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2)
{
	POINT pt;
	RECT rect;
	GetWindowRect(&rect);
	GetCursorPos(&pt);
	if (nHotKeyId >= 1 && nHotKeyId <= 14)
	{
		int item_Id = nHotKeyId + 1003;
		HWND hwnd = ::GetDlgItem(m_dialog1Hwnd, item_Id);
		HDC dc = ::GetDC(this->m_hWnd);
		COLORREF rgb = GetPixel(dc, pt.x - rect.left, pt.y - rect.top);

		WCHAR wch[255];
		wsprintf(wch, L"%d,%d,0x%02x%02x%02x", pt.x - rect.left, pt.y - rect.top, GetRValue(rgb), GetGValue(rgb), GetBValue(rgb));
		::SetWindowText(hwnd, wch);
		UpdateData(FALSE);
	}
	switch (nHotKeyId)
	{
	case 21:
		SetCursorPos(pt.x, pt.y - 1);
		break;
	case 22:
		SetCursorPos(pt.x, pt.y + 1);
		break;
	case 23:
		SetCursorPos(pt.x - 1, pt.y);
		break;
	case 24:
		SetCursorPos(pt.x + 1, pt.y);
		break;
	case 25:
		m_isDrawCube = TRUE;
		GetCursorPos(&m_cubeMousePt);
		m_cubeMousePt.x -= rect.left;
		m_cubeMousePt.y -= rect.top;
		break;
	case 26:
		if (m_isDrawCube)
		{
			m_isDrawCube = FALSE;
			::SendMessage(m_dialog1Hwnd, WM_MYCUBEMESSAGE, NULL, (LPARAM)this);
		}
		break;
	default:
		break;
	}

}



void CDialog2::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nChar == VK_CONTROL)
		return;

	CDialogEx::OnKeyDown(nChar, nRepCnt, nFlags);
}
