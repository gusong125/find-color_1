﻿
// FindColorToolDlg.cpp: 实现文件
//

#include "pch.h"
#include "framework.h"
#include "FindColorTool.h"
#include "FindColorToolDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CFindColorToolDlg 对话框



CFindColorToolDlg::CFindColorToolDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_FINDCOLORTOOL_DIALOG, pParent),
	m_targetDC(NULL)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CFindColorToolDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CFindColorToolDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BUTTON1, &CFindColorToolDlg::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CFindColorToolDlg::OnBnClickedButton2)
	ON_WM_KEYDOWN()
END_MESSAGE_MAP()


// CFindColorToolDlg 消息处理程序

BOOL CFindColorToolDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// 设置此对话框的图标。  当应用程序主窗口不是对话框时，框架将自动
	//  执行此操作
	SetIcon(m_hIcon, TRUE);			// 设置大图标
	SetIcon(m_hIcon, FALSE);		// 设置小图标

	// TODO: 在此添加额外的初始化代码

	m_cDialog1.Create(IDD_DIALOG1, NULL);
	m_cDialog2.Create(IDD_DIALOG2, this);
	RECT rect;
	GetWindowRect(&rect);
	m_cDialog2.MoveWindow(100, 10, rect.right - rect.left - 100, rect.bottom - rect.top - 100);
	m_cDialog2.ShowWindow(SW_SHOW);

	m_cDialog2.m_toolHwnd = this->m_hWnd;
	m_cDialog2.m_dialog1Hwnd = m_cDialog1.m_hWnd;
	m_cDialog2.m_rangeSize = MAX_COLOR;
	return TRUE;  // 除非将焦点设置到控件，否则返回 TRUE
}

// 如果向对话框添加最小化按钮，则需要下面的代码
//  来绘制该图标。  对于使用文档/视图模型的 MFC 应用程序，
//  这将由框架自动完成。

void CFindColorToolDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // 用于绘制的设备上下文

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// 使图标在工作区矩形中居中
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// 绘制图标
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

//当用户拖动最小化窗口时系统调用此函数取得光标
//显示。
HCURSOR CFindColorToolDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CFindColorToolDlg::OnBnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_cDialog1.IsWindowVisible())
		m_cDialog1.ShowWindow(SW_HIDE);
	else
		m_cDialog1.ShowWindow(SW_SHOW);
}


void CFindColorToolDlg::OnBnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	if (m_targetDC != NULL)
		DeleteDC(m_targetDC);
	m_targetDC = ::GetDC(m_cDialog1.g_targetHwnd);
	m_cDialog2.m_targerDC = m_targetDC;

	RECT rect, myRect;

	::GetWindowRect(m_cDialog1.g_targetHwnd, &rect);
	GetWindowRect(&myRect);

	if (myRect.bottom - myRect.top < rect.bottom - rect.top + 70
		|| myRect.right - myRect.left < rect.right - rect.left + 130)
		MoveWindow(myRect.left, myRect.top,
			rect.right - rect.left + 130,
			rect.bottom - rect.top + 70);

	::MoveWindow(m_cDialog2.m_hWnd, 100, 10, rect.right - rect.left, rect.bottom - rect.top, TRUE);
	HDC capDC = ::GetDC(m_cDialog2.m_hWnd);
	BitBlt(capDC, 0, 0, rect.right - rect.left, rect.bottom - rect.top,
		m_targetDC, 0, 0, SRCCOPY);
	DeleteDC(capDC);
}


void CFindColorToolDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (nChar == VK_CONTROL)
		return;

	CDialogEx::OnKeyDown(nChar, nRepCnt, nFlags);
}
