﻿#pragma once
#include <vector>

// CDialog2 对话框

class CDialog2 : public CDialogEx
{
	DECLARE_DYNAMIC(CDialog2)

public:
	CDialog2(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~CDialog2();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DIALOG2 };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnHotKey(UINT nHotKeyId, UINT nKey1, UINT nKey2);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	HWND m_toolHwnd, m_dialog1Hwnd;
	int m_rangeSize;
	std::vector<std::vector<RGBTRIPLE>> tempRGB;
	POINT m_curMousePt,m_cubeMousePt;
	BOOL m_isDrawCube;
	HDC m_targerDC;
};
